from tkinter import *
from tkinter import ttk
from tkinter import messagebox
import joblib
import numpy as np
import pandas as pd
import seaborn as sns
from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import train_test_split, learning_curve
from sklearn.pipeline import make_pipeline
from sklearn.feature_selection import SelectKBest, f_classif
from sklearn.metrics import f1_score, confusion_matrix, classification_report, accuracy_score
from sklearn.model_selection import GridSearchCV, RandomizedSearchCV

version = "1.0"
date = "2023-03-30T12:10:53.600z"
opsys = "Windows_NT x64  -  Windows_NT x32"
navbarColor = "#008000"
bgColor = "#EDFFF9"
whiteColor = "#FFFFFF"
blackColor = "#000000"
bgMenu = "#BEFEE0"

model = joblib.load("ia.pkl")

#- FONCTION DE PREDICTION
def predicts(tupl):
    Labels = ["Potatoes", "Maize", "Wheat", "Rice, paddy", "Soybeans", "Sorghum", "Sweet potatoes", "Cassava", "Yams", "Plantains and others"]
    inputs = [tupl[0], tupl[1], tupl[2], tupl[3]]
    predict = Labels[model.predict([inputs])[0]]
    index = Labels.index(predict)
    return predict, index

#- GESTION DES FENETRES MODALES 
def showWindows_showinfo():
    messagebox.showinfo("Info", f"~ Version: {version} (user setup)\n\n~ Date: {date}\n\n~ OS: {opsys}")
def showWindows_showwarning():
    messagebox.showwarning("Warning", "The data is not filled in!")
def showWindows_showerror():
    messagebox.showerror("Error", "An error has been detected!")

#- TEST DE VALIDITE DES VALAURS ENTRE
def update(Temp, Rain, Yiel, Pest, Pred, Labs):
    try:
        Temp, Rain, Yiel, Pest = float(Temp), float(Rain), float(Yiel), float(Pest)
        tupl = (Temp, Rain, Yiel, Pest)
        pred, ind = predicts(tupl)
        LabelsPred = [
            "The leaves (haulms) grow at temperatures between\n7 and 30°C, but their growth is optimal between\n20 and 25°C. After clearing, light plowing is \nsufficient on light soil and deep plowing when the \nsoil is a little heavy. Then follows the \nmaking of mounds 50 - 60 cm wide and 30 - 40\n cm high with a spacing of 1m between \nthem. These mounds are generally 5 to 6 \nm in length.\nOther cultures of the same categor : Potato, onions, beans, sweet potatoes\nPercentage provided by Africa : /", 
            "Maize is a summer crop, sowing should be \ndone as soon as the soil temperature is \nabove 10°C to allow good seed germination. \nThe maize is sown in rows spaced about 75 cm \napart to ensure good sunlight for the \nplants, with one seed every 13 cm on \nhe row for root development. The optimum \nsowing depth is between 4 and 5 cm. Closer \nto the surface, the seed would be more exposed to\n bird attacks and might not germinate \nin the event of dry weather conditions in the days \nfollowing sowing. Conversely, if the seed is sown too \ndeeply, emergence will be slower and less regular.\nOther cultures of the same categor : \nFodder maize, grain maize, sorghum, millet, \nfinger millet, wheat\nPercentage provided by Africa : 9%", 
            "For successful planting, space each grain \nabout 3-4 cm apart and make sure to push \nthe grains lightly into the ground. Then, \ncarefully cover with your rake, then lightly \nwater your seedlings.\nOther cultures of the same categor : Fodder maize, grain \nmaize, sorghum, millet, finger millet\nPercentage provided by Africa : 9%",
            "Rice, paddy : Rice can be grown in cool, \nmarshy or artificially submerged soil, in \nsouthern regions subject to intense sunshine, \nsuch as the Camargue. Its sowing can be started \nin a pot under shelter to save time, because \ngermination requires a temperature of at least 14°C.\nOther cultures of the same categor : Rice\nPercentage provided by Africa : 6%",
            "To grow soybeans, start by tilling the soil \nwith a croc, in order to aerate it. Then form \nfurrows a few cm deep, spaced between them by \n60-80cm. Place a seed every 5cm in the furrow, \nthen cover with soil to close it. Soybeans are harvested \nat the end of summer. \nOther cultures of the same categor : Wheat\nPercentage provided by Africa : 24,94%",
            "Sowing is done around May-June: the sorghum \nseeds are planted about 3-4 cm deep, spacing the \nplants 40 cm apart and the rows 60 cm apart. \nYou will tamp the soil after covering the seeds \nand you will water. \nOther cultures of the same categor : Maize, wheat, millet\nPercentage provided by Africa : 9%", 
            "Plant in cool, deep, humus-rich soil with a \nsunny exposure. This potato is very suitable for \ncultivation in southern regions, elsewhere we \nrecommend that you cultivate it under possibly \nheated shelters to obtain a good production.\nOther cultures of the same categor : Potato, cucumber, \ncarrots, bethérave\nPercentage provided by Africa : /", 
            "Cassava likes light, well-drained, deep soils \nrich in organic matter. It can grow on all soils \nexcept heavy and flooded ones. Agronomists estimate \nthat the optimum yield of cassava is obtained \nwhen the rainfall is between 1200 and 1500 mm, \nwith an average temperature of 23°C to 24°C. In \nCameroon, cassava is grown in the forest zone, in the coastal plain and in the Adamaoua plateau. \nOther cultures of the same categor : Yam, corn; the vegetable, banana, plantain\nPercentage provided by Africa : 19%", 
            "It is advisable to sow at minimum spacings of \n1 m*1 m, which will result in a density of 10,000 mounds/ha; \nbut the best yields are obtained at \nspacings of 1.5 m*1.5 m (4,444 mounds/ha) because \nit allows the formation of larger mounds, more resistant \nto heavy rains and which can take up to 04 fragments. \nOther cultures of the same categor : Peanut, legume\nPercentage provided by Africa : 70%", 
            "The plantain, producing during its life an important \nbiomass, needs large amounts of mineral and organic \nelements. The Soil must therefore be \nwell provided with these elements. As it concerns the \nclimate, the banana tree needs a constant and high \ntemperature around 27°C all year round.\nOther cultures of the same categor : Banana\nPercentage provided by Africa : 30%"
        ]
        advice = LabelsPred[ind]
        Pred.set(f"prediction results : {pred}")
        Labs.set(advice)
        return pred
    except ValueError:
        return showWindows_showerror()

#- OBSERVEUR DE ENTREES
def getVar(*args):
    pass

#- RRAFFRECHISSEMENT DES ENTRY
def clear_button_operation():
    varTemp.set("")
    varRain.set("")
    varYiel.set("")
    varPest.set("")
    varPred.set("")
    varLabs.set("")


#- L'IA (MODEL + PREDICTION)
def ia(*args):
    if varTemp.get() == "" or varRain.get() == "" or varYiel.get() == "" or varPest.get() == "":
        return showWindows_showwarning()
    else:
        return update(Temp = varTemp.get(), Rain = varRain.get(), Yiel = varYiel.get(), Pest = varPest.get(), Pred = varPred, Labs = varLabs)

#- APPLICATION FENETRE
app = Tk()
w, h = (app.winfo_screenwidth() * 4) // 5, (app.winfo_screenheight() * 4) // 5
app.resizable(width = False, height = False)
app.minsize(w, h)
posX, posY = (int(app.winfo_screenwidth()) // 2) - (w // 2), (int(app.winfo_screenheight()) // 2) - (h // 2)
geometry = "{}x{}+{}+{}".format(w, h, posX, posY)
app.geometry(geometry)
app.title("agriAid")

#- ICONE PAR DEFAU
app.iconbitmap("icon.ico")

#- MENUS DE NAVIGATION
appMenu = Menu()
firstMenu = Menu(appMenu, tearoff = 0, bg=bgMenu, font=("Cascadia Code", 12))
firstMenu.add_command(label = "Version", command = showWindows_showinfo)
appMenu.add_cascade(label = "Help", menu = firstMenu)
secondMenu = Menu(appMenu, tearoff = 0, bg=bgMenu, font=("Cascadia Code", 12))
secondMenu.add_command(label = "Quit", command = app.quit)
appMenu.add_cascade(label = "Exit", menu = secondMenu)
app.config(menu = appMenu)

#- NAVBAR ##########
navbar = Frame(app, bd=5, bg=navbarColor, relief=GROOVE)
navbar.pack(side=TOP, fill=X)
logo = PhotoImage(file="logo.png")
nav_logo = Label(navbar, image=logo)
nav_logo.grid(rowspan=2, column=1, padx=0)
nav_brand = Label(navbar, text="Aid of agriculture",font=("Cascadia Code", 25, "bold"),bg = navbarColor, fg=whiteColor, anchor="w")
nav_brand.grid(row=0, column=2, padx=0)
nav_link = Label(navbar, text="Raising the bar for sublime yield in Africa",font=("Cascadia Code", 12, "bold"),bg = navbarColor, fg=whiteColor, anchor="w")
nav_link.grid(row=1, column=2, padx=0)

#- FENETRE D'EVALUATION DES PARAMETRES DE PREDICTION
params_frame = Frame(app,bd=5, bg=bgColor, relief=GROOVE)
params_frame.place(x=0,y=90,height=(h-110),width=(w//2))

params_frame_title = Label(params_frame, text="Enter your parameters", font=("Cascadia Code", 30, "bold"),bg = bgColor, fg=blackColor)
params_frame_title.pack(side=TOP,fill=X, pady=20)

params_frame_display = Frame(params_frame, bg=bgColor)
params_frame_display.pack(fill=X)

spaceX, spaceY = (w // 25), (h // 25)

varTemp = StringVar()
varTemp.set("")
varTemp.trace("r", getVar)
params_frame_temp_label = Label(params_frame_display, text="Average Temperatures in °C", font=("Cascadia Code", 12, "bold"),bg = bgColor, fg=blackColor)
params_frame_temp_label.grid(row=0,column=0, padx=spaceX, pady=spaceY)
params_frame_temp_entry = Entry(params_frame_display, font=("Cascadia Code", 12),textvariable=varTemp, width=10)
params_frame_temp_entry.grid(row=0,column=1,padx=spaceX, pady=spaceY)

varRain = StringVar()
varRain.set("")
varRain.trace("r", getVar)
params_frame_rain_label = Label(params_frame_display, text="Average Rain Fall mm Per Year", font=("Cascadia Code", 12, "bold"),bg = bgColor, fg=blackColor)
params_frame_rain_label.grid(row=1,column=0, padx=spaceX, pady=spaceY)
params_frame_rain_entry = Entry(params_frame_display, font=("Cascadia Code", 12),textvariable=varRain, width=10)
params_frame_rain_entry.grid(row=1,column=1,padx=spaceX, pady=spaceY)

varYiel = StringVar()
varYiel.set("")
varYiel.trace("r", getVar)
params_frame_area_label = Label(params_frame_display, text="Pending Yield (hg/hg)", font=("Cascadia Code", 12, "bold"),bg = bgColor, fg=blackColor)
params_frame_area_label.grid(row=2,column=0, padx=spaceX, pady=spaceY)
params_frame_area_entry = Entry(params_frame_display, font=("Cascadia Code", 12),textvariable=varYiel, width=10)
params_frame_area_entry.grid(row=2,column=1,padx=spaceX, pady=spaceY)

varPest = StringVar()
varPest.set("")
varPest.trace("r", getVar)
params_frame_pest_label = Label(params_frame_display, text="Pesticides in tons", font=("Cascadia Code", 12, "bold"),bg = bgColor, fg=blackColor)
params_frame_pest_label.grid(row=3,column=0, padx=spaceX, pady=spaceY)
params_frame_pest_entry = Entry(params_frame_display, font=("Cascadia Code", 12),textvariable=varPest, width=10)
params_frame_pest_entry.grid(row=3,column=1,padx=spaceX, pady=spaceY)


pred_button = ttk.Button(params_frame_display, text="Predict", command=ia)
pred_button.grid(row=4,column=0,padx=spaceX,pady=(spaceY*3))
clear_button = ttk.Button(params_frame_display, text="Clear",command=clear_button_operation)
clear_button.grid(row=4,column=1,padx=spaceX,pady=(spaceY*3))

#- FENETRE IMAGE 
predict_frame = Frame(app,bd=5, bg=bgColor, relief=GROOVE)
predict_frame.place(x=(w//2),y=90,height=(h-110),width=(w//2))

can = Canvas(predict_frame, width=(w//2), height=((h-110)//3), bg=bgColor)
can = Canvas(predict_frame, width=(w//2), height=((h-110)//3), bg=bgColor)
can.pack(side=TOP)
hero = PhotoImage(file="hero.png")
hero_frame = Label(can, image=hero)
hero_frame.pack()

#- DES CULTURES ET DES CONSEILS
predict_canvas = Canvas(predict_frame, bg=bgColor, width=(w//2), height=(h - (h-110)//3), scrollregion=(0,0,500,500))
varPred = StringVar()
varPred.set("")
varPred.trace("r", getVar)
predict_canvas_title = Label(predict_canvas, textvariable=varPred, font=("Cascadia Code", 25, "bold"),bg = bgColor, fg=blackColor)
predict_canvas_title.pack(side=TOP,fill=X, pady=5)

varLabs = StringVar()
varLabs.set("")
varLabs.trace("r", getVar)
predict_canvas_msg = Label(predict_canvas, textvariable=varLabs, font=("Cascadia Code", 11, "bold"),bg = bgColor, fg=blackColor)
predict_canvas_msg.pack(side=TOP,fill=X)

predict_canvas.pack()


app.mainloop()